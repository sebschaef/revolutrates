package de.sebschaef.revolutrates

import de.sebschaef.revolutrates.model.network.LatestRatesResponse
import de.sebschaef.revolutrates.model.network.RatesResponse
import de.sebschaef.revolutrates.model.persistence.CurrencyAbbreviations
import de.sebschaef.revolutrates.network.toCurrencyRateList
import org.junit.Test


class ResponseMapperTest {

    // list of valid numbers
    private val ratesResponse = RatesResponse(
        1.0,
        56.0,
        Double.MAX_VALUE,
        Double.MIN_VALUE,
        1234.5,
        Math.PI,
        Int.MAX_VALUE.toDouble(),
        5.14334,
        12.0,
        345.77876,
        40.14334,
        112.0,
        345.77876,
        40.14334,
        12.0,
        345.77876,
        43.14334,
        12.0,
        365.77876,
        40.1345564,
        12.0,
        345.77876,
        40.84334,
        12.0,
        345.77876,
        550.14334,
        4.0,
        345.77876,
        4.14334,
        12.0,
        345.77876,
        3.4756
    )

    @Test
    fun `test null value is not included in the resulting list`() {
        val listWithNullValue = ratesResponse.copy(nzd = null)
        val latestRatesResponse = LatestRatesResponse(CurrencyAbbreviations.EUR, listWithNullValue)

        assert(
            latestRatesResponse.toCurrencyRateList()
                .any { it.currency.currencyCode == CurrencyAbbreviations.NZD }
                .not()
        )
    }

    @Test
    fun `test negative rates are not included in the resulting list`() {
        val listWithNegativeValue = ratesResponse.copy(gbp = -5.0)
        val latestRatesResponse = LatestRatesResponse(CurrencyAbbreviations.KRW, listWithNegativeValue)

        assert(
            latestRatesResponse.toCurrencyRateList()
                .firstOrNull { it.currency.currencyCode == CurrencyAbbreviations.GBP } == null
        )
    }

    @Test
    fun `test base currency is not included twice in the resulting list`() {
        val latestRatesResponse = LatestRatesResponse(CurrencyAbbreviations.NOK, ratesResponse.copy())

        assert(
            latestRatesResponse.toCurrencyRateList()
                .count { it.currency.currencyCode == CurrencyAbbreviations.NOK } == 1
        )
    }

    @Test
    fun `test base currency is first element in the resulting list`() {
        val latestRatesResponse = LatestRatesResponse(CurrencyAbbreviations.DKK, ratesResponse.copy())

        assert(
            latestRatesResponse.toCurrencyRateList()
                .indexOfFirst { it.currency.currencyCode == CurrencyAbbreviations.DKK } == 0
        )
    }

    @Test
    fun `test base currency has 1 as its rate`() {
        val latestRatesResponse = LatestRatesResponse(CurrencyAbbreviations.BRL, ratesResponse.copy())

        assert(
            latestRatesResponse.toCurrencyRateList()
                .firstOrNull { it.currency.currencyCode == CurrencyAbbreviations.BRL }
                ?.rate == 1.0
        )
    }

}