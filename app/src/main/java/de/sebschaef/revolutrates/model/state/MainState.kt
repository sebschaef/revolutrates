package de.sebschaef.revolutrates.model.state

import de.sebschaef.revolutrates.model.persistence.CurrencyRate

sealed class MainState {
    object Initial : MainState()

    data class CurrencyRatesList(
        val currencyRates: List<CurrencyRate>
    ) : MainState()

    data class Error(
        val throwable: Throwable
    ) : MainState()
}