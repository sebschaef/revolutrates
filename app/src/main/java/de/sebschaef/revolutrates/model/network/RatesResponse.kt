package de.sebschaef.revolutrates.model.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import de.sebschaef.revolutrates.model.persistence.CurrencyAbbreviations

@JsonClass(generateAdapter = true)
data class RatesResponse(
    @Json(name = CurrencyAbbreviations.EUR) val eur: Double?,
    @Json(name = CurrencyAbbreviations.AUD) val aud: Double?,
    @Json(name = CurrencyAbbreviations.BGN) val bgn: Double?,
    @Json(name = CurrencyAbbreviations.BRL) val brl: Double?,
    @Json(name = CurrencyAbbreviations.CAD) val cad: Double?,
    @Json(name = CurrencyAbbreviations.CHF) val chf: Double?,
    @Json(name = CurrencyAbbreviations.CNY) val cny: Double?,
    @Json(name = CurrencyAbbreviations.CZK) val czk: Double?,
    @Json(name = CurrencyAbbreviations.DKK) val dkk: Double?,
    @Json(name = CurrencyAbbreviations.GBP) val gbp: Double?,
    @Json(name = CurrencyAbbreviations.HKD) val hkd: Double?,
    @Json(name = CurrencyAbbreviations.HRK) val hrk: Double?,
    @Json(name = CurrencyAbbreviations.HUF) val huf: Double?,
    @Json(name = CurrencyAbbreviations.IDR) val idr: Double?,
    @Json(name = CurrencyAbbreviations.ILS) val ils: Double?,
    @Json(name = CurrencyAbbreviations.INR) val inr: Double?,
    @Json(name = CurrencyAbbreviations.ISK) val isk: Double?,
    @Json(name = CurrencyAbbreviations.JPY) val jpy: Double?,
    @Json(name = CurrencyAbbreviations.KRW) val krw: Double?,
    @Json(name = CurrencyAbbreviations.MXN) val mxn: Double?,
    @Json(name = CurrencyAbbreviations.MYR) val myr: Double?,
    @Json(name = CurrencyAbbreviations.NOK) val nok: Double?,
    @Json(name = CurrencyAbbreviations.NZD) val nzd: Double?,
    @Json(name = CurrencyAbbreviations.PHP) val php: Double?,
    @Json(name = CurrencyAbbreviations.PLN) val pln: Double?,
    @Json(name = CurrencyAbbreviations.RON) val ron: Double?,
    @Json(name = CurrencyAbbreviations.RUB) val rub: Double?,
    @Json(name = CurrencyAbbreviations.SEK) val sek: Double?,
    @Json(name = CurrencyAbbreviations.SGD) val sgd: Double?,
    @Json(name = CurrencyAbbreviations.THB) val thb: Double?,
    @Json(name = CurrencyAbbreviations.USD) val usd: Double?,
    @Json(name = CurrencyAbbreviations.ZAR) val zar: Double?
)