package de.sebschaef.revolutrates.model.persistence

import java.util.*

data class CurrencyRate(
    val currency: Currency,
    val rate: Double
)