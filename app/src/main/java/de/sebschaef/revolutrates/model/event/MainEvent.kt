package de.sebschaef.revolutrates.model.event

import de.sebschaef.revolutrates.model.persistence.CurrencyRate

sealed class MainEvent {
    object Resume : MainEvent()
    object Pause : MainEvent()

    data class CurrencyRateSelected(
        val currencyRate: CurrencyRate
    ) : MainEvent()

    data class BaseCurrencyRateChanged(
        val rate: Double
    ) : MainEvent()
}