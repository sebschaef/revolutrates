package de.sebschaef.revolutrates.extensions

fun <T> List<T>.shiftToTop(item: T): List<T> =
    if (contains(item)) {
        toMutableList().apply {
            remove(item)
            add(0, item)
        }.toList()
    } else {
        this
    }