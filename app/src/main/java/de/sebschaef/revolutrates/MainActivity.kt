package de.sebschaef.revolutrates

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.sebschaef.revolutrates.adapter.currencyRateAdapter
import de.sebschaef.revolutrates.model.event.MainEvent.*
import de.sebschaef.revolutrates.model.persistence.CurrencyRate
import de.sebschaef.revolutrates.model.state.MainState
import de.sebschaef.revolutrates.model.state.MainState.*

class MainActivity : AppCompatActivity(), MainContract.View {

    // View references
    private val rvCurrencyRatesList by lazy {
        findViewById<RecyclerView>(R.id.rv_currency_rates_list)
    }

    private val errorToast by lazy {
        Toast.makeText(this, R.string.error_unknown, Toast.LENGTH_SHORT)
    }

    // Observers
    private val adapterDataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) {
            super.onItemRangeChanged(positionStart, itemCount, payload)
            if (positionStart == BASE_CURRENCY_POSITION) {
                rvCurrencyRatesList.smoothScrollToPosition(BASE_CURRENCY_POSITION)
                openSoftKeyboardForBaseCurrency()
            }
        }
    }

    private val baseCurrencyTextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            val rate = if (s.isNullOrBlank()) {
                0.0
            } else {
                s.toString().toDoubleOrNull() ?: 1.0
            }
            viewModel.onViewEvent(BaseCurrencyRateChanged(rate))
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
    }

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        var isScrolling = false

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            isScrolling = dy != 0
            if (dy > 0) {
                closeSoftKeyboard()
            } else if (dy < 0 && isBaseCurrencyItemVisible) {
                openSoftKeyboardForBaseCurrency()
            }
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                isScrolling = false
            }
        }
    }

    // Misc
    private val currencyRateAdapter = currencyRateAdapter(baseCurrencyTextWatcher) {
        viewModel.onViewEvent(CurrencyRateSelected(it))
    }

    private val inputMethodManager: InputMethodManager?
        get() = (getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)

    private val isBaseCurrencyItemVisible: Boolean
        get() = (rvCurrencyRatesList.layoutManager as? LinearLayoutManager)
            ?.findFirstVisibleItemPosition() == BASE_CURRENCY_POSITION

    private val viewModel: MainViewModel = MainViewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        initCurrencyRatesRecyclerView()
    }

    override fun onResume() {
        super.onResume()
        startObservingViewState()
        viewModel.onViewEvent(Resume)
    }

    override fun onPause() {
        closeSoftKeyboard()
        super.onPause()
        viewModel.onViewEvent(Pause)
        stopObservingViewState()
    }

    override fun onStop() {
        super.onStop()
        resetCurrencyRatesRecyclerView()
    }

    private fun initCurrencyRatesRecyclerView() {
        rvCurrencyRatesList.apply {
            setHasFixedSize(true)
            adapter = currencyRateAdapter
            adapter?.registerAdapterDataObserver(adapterDataObserver)
            addOnScrollListener(scrollListener)
        }
    }

    private fun resetCurrencyRatesRecyclerView() {
        rvCurrencyRatesList.adapter?.unregisterAdapterDataObserver(adapterDataObserver)
        rvCurrencyRatesList.adapter = null
    }

    private fun startObservingViewState() {
        viewModel.state.observe(this) { render(it) }
    }

    private fun stopObservingViewState() {
        viewModel.state.removeObservers(this)
    }

    override fun render(state: MainState) = when (state) {
        is Initial -> Unit
        is CurrencyRatesList -> updateCurrencyRateList(state.currencyRates)
        is Error -> errorToast.show()
    }

    private fun updateCurrencyRateList(currencyRates: List<CurrencyRate>) {
        errorToast.cancel()
        if (!scrollListener.isScrolling) {
            currencyRateAdapter.items = currencyRates
        }
    }

    private fun openSoftKeyboardForBaseCurrency() {
        rvCurrencyRatesList.layoutManager
            ?.findViewByPosition(BASE_CURRENCY_POSITION)
            ?.findViewById<EditText>(R.id.et_currency_rate)
            ?.let {
                it.requestFocus()
                inputMethodManager?.showSoftInput(it, InputMethodManager.SHOW_IMPLICIT)
            }
    }

    private fun closeSoftKeyboard() {
        currentFocus?.let {
            inputMethodManager?.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    companion object {
        const val BASE_CURRENCY_POSITION = 0
    }
}