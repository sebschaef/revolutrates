package de.sebschaef.revolutrates.repository

import de.sebschaef.revolutrates.model.persistence.CurrencyRate
import de.sebschaef.revolutrates.network.RevolutService
import de.sebschaef.revolutrates.network.toCurrencyRateList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RepositoryImpl : Repository.Api {

    val api: Repository.Api = this

    private const val REVOLUT_SERVICE_BASE_URL = "https://hiring.revolut.codes/"

    // API
    private val revolutService by lazy {
        Retrofit.Builder()
            .baseUrl(REVOLUT_SERVICE_BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(RevolutService::class.java)
    }

    override fun getLatestRates(
        base: String,
        coroutineScope: CoroutineScope,
        onSuccess: (List<CurrencyRate>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        coroutineScope.launch(Dispatchers.IO) {
            try {
                val currencyRates = revolutService.getLatestRates(base).toCurrencyRateList()
                withContext(Dispatchers.Main) { onSuccess(currencyRates) }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) { onError(e) }
            }
        }
    }

}