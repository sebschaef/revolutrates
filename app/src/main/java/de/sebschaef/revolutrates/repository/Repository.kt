package de.sebschaef.revolutrates.repository

import de.sebschaef.revolutrates.model.persistence.CurrencyRate
import kotlinx.coroutines.CoroutineScope

interface Repository {

    interface Api {
        fun getLatestRates(
            base: String,
            coroutineScope: CoroutineScope,
            onSuccess: (List<CurrencyRate>) -> Unit,
            onError: (Throwable) -> Unit
        )
    }

}