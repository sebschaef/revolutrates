package de.sebschaef.revolutrates

import androidx.lifecycle.LiveData
import de.sebschaef.revolutrates.model.event.MainEvent
import de.sebschaef.revolutrates.model.state.MainState

interface MainContract {

    interface Controller {
        val state: LiveData<MainState>
        fun onViewEvent(event: MainEvent)
    }

    interface View {
        fun render(state: MainState)
    }

}