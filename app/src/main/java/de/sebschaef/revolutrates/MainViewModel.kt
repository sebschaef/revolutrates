package de.sebschaef.revolutrates

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.sebschaef.revolutrates.extensions.shiftToTop
import de.sebschaef.revolutrates.model.event.MainEvent
import de.sebschaef.revolutrates.model.event.MainEvent.*
import de.sebschaef.revolutrates.model.persistence.CurrencyAbbreviations
import de.sebschaef.revolutrates.model.persistence.CurrencyRate
import de.sebschaef.revolutrates.model.state.MainState
import de.sebschaef.revolutrates.model.state.MainState.*
import de.sebschaef.revolutrates.repository.Repository
import de.sebschaef.revolutrates.repository.RepositoryImpl

class MainViewModel : ViewModel(), MainContract.Controller {

    private val repositoryApi: Repository.Api = RepositoryImpl.api

    override val state = MutableLiveData<MainState>(Initial)

    private var currentBaseCurrency = CurrencyAbbreviations.EUR
    private var currentBaseCurrencyRate = 1.0
    private var currentOriginalRates: List<CurrencyRate>? = null

    private val handler = Handler()
    private lateinit var fetchCurrencyRatesRunnable: Runnable

    override fun onViewEvent(event: MainEvent) = when (event) {
        is Resume -> startFetchingCurrencyRates()
        is Pause -> stopFetchingCurrencyRates()
        is CurrencyRateSelected -> onCurrencyRateSelected(event.currencyRate)
        is BaseCurrencyRateChanged -> onBaseCurrencyRateChanged(event.rate)
    }

    private fun startFetchingCurrencyRates() {
        fetchCurrencyRatesRunnable = Runnable {
            repositoryApi.getLatestRates(
                base = currentBaseCurrency,
                coroutineScope = viewModelScope,
                onSuccess = this::onCurrencyRatesReceived,
                onError = { state.value = Error(it) }
            )
            handler.postDelayed(fetchCurrencyRatesRunnable, PERIODIC_REFRESH_TIME_MS)
        }.also { handler.post(it) }
    }

    private fun stopFetchingCurrencyRates() {
        handler.removeCallbacksAndMessages(null)
    }

    private fun onCurrencyRatesReceived(currencyRates: List<CurrencyRate>) {
        currentOriginalRates = currencyRates
        val multipliedRates = currencyRates.map {
            if (it.currency.currencyCode != currentBaseCurrency) {
                it.copy(rate = it.rate * currentBaseCurrencyRate)
            } else {
                it
            }
        }
        state.value = CurrencyRatesList(multipliedRates)
    }

    private fun onCurrencyRateSelected(currencyRate: CurrencyRate) {
        currentBaseCurrency = currencyRate.currency.currencyCode
        currentBaseCurrencyRate = 1.0
        shiftCurrencyRateToTop(currencyRate)
    }

    private fun shiftCurrencyRateToTop(currencyRate: CurrencyRate) {
        val currentState = state.value
        if (currentState is CurrencyRatesList) {
            state.value = currentState.copy(
                currencyRates = currentState.currencyRates.shiftToTop(currencyRate)
            )
        }
    }

    private fun onBaseCurrencyRateChanged(rate: Double) {
        currentBaseCurrencyRate = rate
        recalculateRates()
    }

    private fun recalculateRates() {
        val currentState = state.value
        if (currentState is CurrencyRatesList) {
            currentOriginalRates?.let { onCurrencyRatesReceived(it) }
        }
    }

    companion object {
        const val PERIODIC_REFRESH_TIME_MS = 1000L
    }
}