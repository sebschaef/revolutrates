package de.sebschaef.revolutrates.adapter

import android.text.TextWatcher
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import de.sebschaef.revolutrates.MainActivity
import de.sebschaef.revolutrates.R
import de.sebschaef.revolutrates.model.persistence.CurrencyRate
import java.util.*

private fun Double.toTwoDecimalString(): String = "%.2f".format(Locale.US, this)

private val diffUtil = object : DiffUtil.ItemCallback<CurrencyRate>() {
    override fun areItemsTheSame(oldItem: CurrencyRate, newItem: CurrencyRate) =
        oldItem.currency.currencyCode == newItem.currency.currencyCode

    override fun areContentsTheSame(oldItem: CurrencyRate, newItem: CurrencyRate) =
        oldItem.rate.toTwoDecimalString() == newItem.rate.toTwoDecimalString()
}

fun currencyRateAdapter(
    baseCurrencyTextWatcher: TextWatcher,
    onItemClicked: (CurrencyRate) -> Unit
) = object : AsyncListDifferDelegationAdapter<CurrencyRate>(diffUtil) {
    init {
        delegatesManager.addDelegate(
            adapterDelegate<CurrencyRate, CurrencyRate>(R.layout.item_currency_rate) {
                val ivCurrencyFlag: ImageView = findViewById(R.id.iv_currency_flag)
                val tvCurrencyAbbreviation: TextView =
                    findViewById(R.id.tv_currency_abbreviation)
                val tvCurrencyName: TextView = findViewById(R.id.tv_currency_name)
                val etCurrencyRate: EditText = findViewById(R.id.et_currency_rate)

                bind {
                    tvCurrencyAbbreviation.text = item.currency.currencyCode
                    tvCurrencyName.text = item.currency.displayName

                    bindCurrencyImage(item.currency.currencyCode, ivCurrencyFlag)
                    bindCurrencyRateEditText(item.rate, adapterPosition, etCurrencyRate)

                    itemView.setOnClickListener {
                        onItemClicked(item)
                    }
                }
            }
        )
    }

    private fun bindCurrencyImage(currencyCode: String, imageView: ImageView) {
        val imageResId = imageView.context.resources.getIdentifier(
            currencyCode.toLowerCase(Locale.ENGLISH),
            "drawable",
            imageView.context.packageName
        )

        Glide.with(imageView)
            .load(if (imageResId == 0) R.drawable.ic_failed_to_load else imageResId)
            .circleCrop()
            .into(imageView)
    }

    private fun bindCurrencyRateEditText(
        rate: Double,
        adapterPosition: Int,
        editText: EditText
    ) = editText.apply {
        if (adapterPosition == MainActivity.BASE_CURRENCY_POSITION) {
            isEnabled = true
            addTextChangedListener(baseCurrencyTextWatcher)
        } else {
            isEnabled = false
            removeTextChangedListener(baseCurrencyTextWatcher)
        }
        setText(rate.toTwoDecimalString(), TextView.BufferType.EDITABLE)
    }
}

