package de.sebschaef.revolutrates.network

import de.sebschaef.revolutrates.model.network.LatestRatesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutService {

    @GET("api/android/latest")
    suspend fun getLatestRates(
        @Query("base") base: String
    ): LatestRatesResponse

}