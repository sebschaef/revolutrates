package de.sebschaef.revolutrates.network

import de.sebschaef.revolutrates.model.network.LatestRatesResponse
import de.sebschaef.revolutrates.model.persistence.CurrencyAbbreviations
import de.sebschaef.revolutrates.model.persistence.CurrencyRate
import java.util.*

fun LatestRatesResponse.toCurrencyRateList(): List<CurrencyRate> =
    listOf(
        CurrencyAbbreviations.EUR to rates.eur,
        CurrencyAbbreviations.AUD to rates.aud,
        CurrencyAbbreviations.BGN to rates.bgn,
        CurrencyAbbreviations.BRL to rates.brl,
        CurrencyAbbreviations.CAD to rates.cad,
        CurrencyAbbreviations.CHF to rates.chf,
        CurrencyAbbreviations.CNY to rates.cny,
        CurrencyAbbreviations.CZK to rates.czk,
        CurrencyAbbreviations.DKK to rates.dkk,
        CurrencyAbbreviations.GBP to rates.gbp,
        CurrencyAbbreviations.HKD to rates.hkd,
        CurrencyAbbreviations.HRK to rates.hrk,
        CurrencyAbbreviations.HUF to rates.huf,
        CurrencyAbbreviations.IDR to rates.idr,
        CurrencyAbbreviations.ILS to rates.ils,
        CurrencyAbbreviations.INR to rates.inr,
        CurrencyAbbreviations.ISK to rates.isk,
        CurrencyAbbreviations.JPY to rates.jpy,
        CurrencyAbbreviations.KRW to rates.krw,
        CurrencyAbbreviations.MXN to rates.mxn,
        CurrencyAbbreviations.MYR to rates.myr,
        CurrencyAbbreviations.NOK to rates.nok,
        CurrencyAbbreviations.NZD to rates.nzd,
        CurrencyAbbreviations.PHP to rates.php,
        CurrencyAbbreviations.PLN to rates.pln,
        CurrencyAbbreviations.RON to rates.ron,
        CurrencyAbbreviations.RUB to rates.rub,
        CurrencyAbbreviations.SEK to rates.sek,
        CurrencyAbbreviations.SGD to rates.sgd,
        CurrencyAbbreviations.THB to rates.thb,
        CurrencyAbbreviations.USD to rates.usd,
        CurrencyAbbreviations.ZAR to rates.zar
    )
        .filter { it.second != null && it.second!! >= 0 }
        .map { CurrencyRate(Currency.getInstance(it.first), it.second!!) }
        .sortedBy { it.currency.currencyCode }
        .toMutableList()
        .also {
            val baseCurrencyRate = CurrencyRate(Currency.getInstance(baseCurrency), 1.0)
            it.add(0, baseCurrencyRate)
        }
        .distinctBy { it.currency.currencyCode }